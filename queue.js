let collection = [];

// Write the queue functions below.

function print() {
	return collection;
}

function enqueue(element) {
	// add an element to the rear of the queue
	collection.push(element);
	return collection;
}

function dequeue() {
	// remove an element at the front of the queue

	collection.shift();
	return collection;
}

function front() {
	// show the element at the front of the queue

	return collection[0];
}

function size() {
	return collection.length;
}

function isEmpty() {
	// return a Boolean value describing whether the queue is empty or not

	if (collection == 0) {
		return true;
	} else {
		return false;
	}
}

console.log(isEmpty());

module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty,
};
